public class Printer {
//urok Denis I
    
        /**
         * @param args argument
         */
        public static void main(String[] args) {
            // initialize simple string array
            String[] name = new String[5];

            name[0] = "Denis Ilyin";
            name[1] = "Sergey";
            name[2] = "Denis Ivanov";
            name[3] = "Rimma";
            name[4] = "Test student";

            for (int i = 0; i < name.length; i += 1) {
                System.out.printf("%d - %s\n", i, name[i]);
            }

            System.out.printf("%d - %s\n", 2, name[2]);

            // initialize simple 2d array
            Integer[][] test2d = new Integer[4][5];

            for (int i = 0; i < 4; ++i) {
                for (int j = 0; j < 5; ++j) {
                    test2d[i][j] = (i + j) * (i - j);
                }
            }

            for (int i = 0; i < test2d.length; ++i) {
                for (int j = 0; j < test2d[i].length; ++j) {
                    System.out.printf("%d ", test2d[i][j]);
                }
                System.out.println();
            }

            System.out.println("new test!");
            String[] s = {
                    "12", "456", "789"
            };

            Integer[] testI = {1, 2, 7};
            for (Integer currElement : testI) {
                System.out.println(currElement);
            }

            System.out.println(name);
        }
    }

