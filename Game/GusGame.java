package game;

import java.util.Random;

public class GusGame {
    Player p1;
    Player p2;
    Player p3;

    public void StartGame (){
        p1 = new Player();
        p2 = new Player();
        p3 = new Player();

        int guessp1 = 0;
        int guessp2 = 0;
        int guessp3 = 0;

        boolean p1isRight = false;
        boolean p2isRight = false;
        boolean p3isRight = false;

        int targetNumber = (int) (Math.random() *  21);
        System.out.println("Я загадал число от 0 до 20");
        System.out.println();
        System.out.println("Нажми Enter для продолжения...");
        try
        {
            System.in.read();
        }
        catch(Exception e)
        {}

        while ( true)  {
            System.out.println("Число которое надо отгадать - " + targetNumber);
            System.out.println();

            p1.guess();
            p2.guess();
            p3.guess();

            guessp1 = p1.number;
            System.out.println("Первый игрок думает " + guessp1);

            guessp2 = p2.number;
            System.out.println("Второй игрок думает " + guessp2);

            guessp3 = p3.number;
            System.out.println("Третий игрок думает " + guessp3);

            System.out.println();

            if (guessp1 == targetNumber) {
                p1isRight = true;
            }

            if (guessp2 == targetNumber) {
                p2isRight = true;
            }

            if (guessp3 == targetNumber) {
                p3isRight = true;
            }

//            System.out.println("У нас есть победитель!");


            System.out.println();

            if (p1isRight || p2isRight || p3isRight) {
                System.out.println("У нас есть победитель!");


                if (p1isRight==true){
                    System.out.println("Первый игрок УГАДАЛ!!!");
                }

                if (p2isRight==true){
                    System.out.println("Второй игрок УГАДАЛ!!!");
                }

                if (p3isRight==true){
                    System.out.println("Третий игрок УГАДАЛ!!!");
                }

//
//
//                System.out.println("Первый игрок угадал?" + p1isRight);
//                System.out.println("Второй игрок угадал?" + p2isRight);
//                System.out.println("Третий игрок угадал?" + p3isRight);
                System.out.println();
                System.out.println("Game Over!");



                break;
            }
            else {

                System.out.println("Попробуйте ещё раз");
                System.out.println();
                System.out.println("Нажми Enter для продолжения...");
                try
                {
                    System.in.read();
                }
                catch(Exception e)
                {}

                System.out.println();
            }
        }

    }
}
